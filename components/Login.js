import React, { Component } from 'react';
import logo from '../images/logo.png';
import { Container, Form, Button } from 'react-bootstrap';

class Login extends Component {
    render() {
        return (
            <div className="centerform login">
                <Container>
                    <img src={logo} alt="Logo" />
                    <div className="erros">

                    </div>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control type="email" placeholder="Email:" name="email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Control type="password" placeholder="Password:" name="password" />
                        </Form.Group>
                        <Button className="btn-large" type="submit">Sign In</Button>
                    </Form>
                </Container>
            </div>
        )
    }
}

export default Login