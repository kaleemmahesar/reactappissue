import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { Container, Row, Col, Button } from 'react-bootstrap';
import logo from '../images/logo.png';
import Login from './Login';

const LandingPage = () => {
    return (
        <div className="centerform landing">
            <Container>
                <Row>
                    <Col md={12}>
                        <img src={logo} alt="Logo" />
                        <Link to="/login">Login</Link>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default LandingPage