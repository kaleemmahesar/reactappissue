import React from 'react';
import './App.css';
import './App.scss';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import LandingPage from './components/LandingPage'
import Login from './components/Login';

function App() {
  return (
    <div className="App">
      <Router>
        <LandingPage />
        <Switch>
          <Route exact path="/"><LandingPage /></Route>
          <Route path="/login"><Login /></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
